# OpenML dataset: witmer_census_1980

https://www.openml.org/d/668

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

A shar archive of data from the book Data Analysis: An
Introduction(1992) Prentice Hall bu Jeff Witmer. Submitted by
Jeff Witmer (fwitmer@ocvaxa.cc.oberlin.edu) [28/Jun/94] (29
kbytes)

Note: description taken from this web site:
http://lib.stat.cmu.edu/datasets/

File: ../data/witmer/DATA_FILES/Census_1980


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/668) of an [OpenML dataset](https://www.openml.org/d/668). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/668/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/668/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/668/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

